setopt PROMPT_SUBST

parseGitBranch() {
    git symbolic-ref --short HEAD 2> /dev/null | sed 's/^/ (/' | sed 's/$/)/'
}

PROMPT='%B%F{red}Adolfo Pereira%f %F{white}in%f %F{blue}%9c%f%F{green}$(parseGitBranch)%f 🐼 %b'
RPROMPT='%F{white}'