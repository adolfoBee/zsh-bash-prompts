Prompts for Zsh and bash without the need of "oh my zsh" and "bash it"

To change colors you can either

**PROMPT=$'\e[0;31m$ \e[0m'**

or (recommended)

**PROMPT=%B%F{red}Hello**

Where

%B = start Bold
%b = finish bold

%F = start Color
%f = finish color

for a list of all the colors please refer to the Zsh wiki here:
https://wiki.archlinux.org/index.php/Zsh#Colors

Note: the sed command adds a space before the branch and put parentheses around it,
please use git symbolic-ref instead of git branch as it returns the current branch
even if there is no commit in the repository.